const basicElement = document.getElementById("basicPrice");
const professionalElement = document.getElementById("professionalPrice");
const masterElement = document.getElementById("masterPrice");

const input = document.getElementById("toggller");

input.addEventListener("click", (event) => {
  if (input.value === "off") {
    event.target.value = "on";
    basicElement.textContent = "$199.99";
    professionalElement.textContent = "$249.99";
    masterElement.textContent = "$399.99";
  } else {
    event.target.value = "off";
    basicElement.textContent = "$19.99";
    professionalElement.textContent = "$24.99";
    masterElement.textContent = "$39.99";
  }
});
